import React, { Component } from 'react';

class TodoList extends Component {

    constructor(props) {
      super(props);
      this.state = {
        inputedvalue: [],
        textvalue : "",
       
      }
      this.handleAddTodoItem = this.handleAddTodoItem.bind(this)
      this.handleChange = this.handleChange.bind(this)
      this.handledelTodoItem = this.handledelTodoItem.bind(this)
    }
    handleChange(e) {
      this.setState({
        textvalue:e.target.value
      })
    }
    handleAddTodoItem() {
      this.state.inputedvalue.push(this.state.textvalue)
      this.setState(
        this.state
      )
      this.setState({
        textvalue:""
      })
    }
    handledelTodoItem(value){
      for(var i = 0; i < this.state.inputedvalue.length; i++){
        if(this.state.inputedvalue[i] === value){
           delete this.state.inputedvalue[i]
        }
      }
      this.setState({
        inputedvalue:this.state.inputedvalue
      })
     
    }
    
    render() {
      let { inputedvalue } = this.state;
      return (
        <div>
          <h1> Todo List</h1>
          <input type="text" onChange={ this.handleChange } value={this.state.textvalue}/>
          <button onClick={this.handleAddTodoItem}>Add Item</button>
          <br/><br/>
          {inputedvalue.map((value) => {
            return (
            <div>
            <input type="text" value={value}/>
            <button onClick={this.handledelTodoItem.bind(this, value)}>Delete Item</button>
            <br/><br/>
            </div>
          )
          }
          )}
        </div>
      )
    }
  
}
export default TodoList;